# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Caarmi, 2022
# Вячеслав Волошин <vol_vel@mail.ru>, 2015
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-16 19:17+0300\n"
"PO-Revision-Date: 2013-07-06 14:51+0000\n"
"Last-Translator: Caarmi, 2022\n"
"Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: wallpaper:37
msgid "antiX Wallpaper"
msgstr "Обои antiX"

#: wallpaper:46
msgid "Error"
msgstr "Ошибка"

#: wallpaper:55
msgid "Successfully updated"
msgstr "Успешно обновлено"

#: wallpaper:198
msgid ""
"This is an antiX application for setting the wallpaper on the preinstalled "
"window managers"
msgstr "Это приложение Antix для установки обоев для предустановленных оконных менеджеров"

#: wallpaper:205
msgid "Select Folder..."
msgstr "Выбор папки..."

#: wallpaper:217
msgid "All Files"
msgstr "Все файлы"

#: wallpaper:242
msgid "Select Image..."
msgstr "Выбор изображения..."

#: wallpaper:254
msgid "Images"
msgstr "Изображения"

#: wallpaper:283
msgid "Select background colour"
msgstr "Выбор цвета фона"

#: wallpaper:453
msgid "Help"
msgstr "Справка"

#: wallpaper:456
msgid "About"
msgstr "О приложении"

#: wallpaper:459
msgid "Default Folder"
msgstr "Папка по умолчанию"

#: wallpaper:462
msgid "Set Image"
msgstr "Установить изображение"

#: wallpaper:465 wallpaper:539
msgid "Select Colour"
msgstr "Выбор цвета"

#: wallpaper:468 wallpaper:545
msgid "Close"
msgstr "Закрыть"

#: wallpaper:480
msgid "Options"
msgstr "Настройки"

#: wallpaper:497
msgid "Static"
msgstr "Постоянный"

#: wallpaper:497
msgid "Random Wallpaper"
msgstr "Случайные обои"

#: wallpaper:497
msgid "Random Wallpaper Timed"
msgstr "Случайные обои по таймеру"

#: wallpaper:499
msgid "No Wallpaper"
msgstr "Нет обоев"

#: wallpaper:511
msgid "Scale"
msgstr "Масштабировать"

#: wallpaper:511
msgid "Centre"
msgstr "По центру"

#: wallpaper:511
msgid "Fill"
msgstr "Растянуть"

#: wallpaper:522
msgid "  Time between wallpaper cycle (Minutes)  "
msgstr "Смена обоев через (мин)"

#: wallpaper:536
msgid "Select Folder"
msgstr "Выбор папки"

#: wallpaper:542
msgid "Select Picture"
msgstr "Выбор изображения"

#: wallpaper:548
msgid "Apply"
msgstr "Применить"
