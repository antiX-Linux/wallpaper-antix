��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    %   �  
        '     8     A     M     V     h     n  
   v     �  
   �  	   �     �     �  
   �     �     �     �               +     @  	   R     \  O   v     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: joyinko <joyinko@azet.sk>, 2023
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Doba pro cycklování tapety (Minuty) O aplikaci Všechny soubory Použít Vycentrovat Zavřít Výchozí složka Chyba Vyplnit Nápověda Obrázky Bez tapety Možnosti Náhodná tapeta Náhodná tapeta + cyklování Měřítko Vybrat barvu Vybrat složku Vybrat složku... Vybrat Obrázek... Vybrat obrázek Vybrat barvu pozadí Nastavit obrázek Statické Uspěšně aktualizováno Tohle je aplikace antiX pro nastavení pozadí v nainstalovaném správci oken. antiX Tapety 