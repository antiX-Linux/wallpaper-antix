��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    2   �          	          $     +     2     J     O     V     \     c     �     �  &   �     �     �     �     �          %     7     P  	   a     k  g   �     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020-2021
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Tempo entre o ciclo do fondo de pantalla (minutos) Sobre Todos os ficheiros Aplicar Centro Cerrar Cartafol predeterminado Erro Encher Axuda Imaxes Non hai un fondo de pantalla Opcións Fondo de pantalla aleatorio Fondo de pantalla cronometrado ao azar Escala Seleccionar cor Seleccionar cartafol Seleccionar cartafol... Seleccionar imaxe... Seleccionar imaxe Seleccionar cor do fondo Establecer imaxe Estático Actualizado con éxito Este é un aplicativo de antiX para configurar a imaxe de fondo nos xestores de lapelas predeterminadas Fondo de pantalla antiX 