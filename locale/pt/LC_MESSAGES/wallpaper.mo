��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    5   �          	          $     ,     3     G  	   L     V     \     d     x     �  &   �     �     �     �     �               &     >  	   L     V  g   l     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Paulo C., 2021
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Tempo entre alteração de imagens de fundo (Minutos) Sobre Todos os ficheiros Aplicar Centrar Fechar Pasta pré-definida Erro Preencher Ajuda Imagens Sem imagem de fundo Opções Imagem de fundo aleatória Imagem de fundo aleatória temporizada Dimensionar Escolher Cor Escolher a Pasta Escolher a Pasta... Escolher a Imagem... Escolher Imagem Escolher a cor do fundo Ativar imagem Estática Atualizado com êxito Esta é uma aplicação do antiX para definir a imagem de fundo nos gestores de janelas pré-instalados Imagem de fundo do antiX 