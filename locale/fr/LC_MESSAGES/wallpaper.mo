��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    9   �  	          	   )     3     :     A     U     \     d     i     p     �     �  "   �     �     �     �          $     ?  +   W     �     �     �  p   �     '                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Wallon Wallon, 2021,2023
Language-Team: French (http://app.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
   Intervalle de rotation des fonds d’écran (Minutes)   À propos Tous les dossiers Appliquer Centre Fermer Dossier par défaut Erreur Remplir Aide Images Pas de fond d’écran Options Fond d’écran aléatoire Fond d’écran aléatoire minuté Échelle Sélectionner la couleur Sélectionner un dossier Sélectionner un dossier... Sélectionner une image... Sélectionner une image Sélectionner la couleur du fond d’écran Régler l’image Statique Mise à jour effectuée Application antiX permettant de configurer le fond d’écran dans les gestionnaires de fenêtre préinstallés. Fond d’écran antiX 