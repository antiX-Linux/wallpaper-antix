��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    +   �     �     �     �                    (     .     4     ;     D     T     ]     q     �     �     �     �     �     �     �       	        "  h   A     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2014-2015,2021
Language-Team: Catalan (http://www.transifex.com/anticapitalista/antix-development/language/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Temps entre els canvis d'empaperat (minuts) Quant a  Tots els fitxers Aplica Centrat Tanca Carpeta per omissió  Error Omple Ajuda  Imatges  Sense empaperat Opcions  Empaperat aleatori  Empaperat aleatori canviant  Escalat Trieu el color Trieu la carpeta  Trieu la carpeta... Trieu la imatge... Trieu la imatge Trieu el color de fons Defineix la imatge Estàtic  Actualització satisfactòria  Aquesta és una aplicació d'antiX per configurar l'empaperat dels gestors de finestres preinstal·lats  antiX Wallpaper 