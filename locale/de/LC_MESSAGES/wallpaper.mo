��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       t    $   �  #   �     �     �     �  
             !     (     H     N     U     j     x  %   �     �     �     �     �                    <  	   K  2   U  u   �     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Robin, 2021
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Intervall des Bildwechsels (Minuten) Informationen über dieses Programm Alle Dateien Anwenden Bild zentrieren Schließen Standardverzeichnis Fehler Bildschirmfüllende Bildgröße Hilfe Bilder Kein Hintergrundbild Einstellungen Zufälliges Hintergrundbild Zufällig wechselndes Hintergrundbild Bildgröße anpassen Farbauswahl Verzeichnisauswahl Verzeichnisauswahl... Bildauswahl... Bildauswahl Auswahl der Hintergrundfarbe Bild festlegen Dauerhaft Aktualisierung der Voreinstellungen abgeschlossen. Mit dieser antiX Anwendung kann das Hintergrundbild für die vorinstallierten Fensterverwaltungen ausgewählt werden. antiX Hintergrundbild 