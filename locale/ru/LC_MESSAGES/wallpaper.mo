��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �            )        I     a     s     �     �  "   �     �     �     �     �          "     5  /   Q     �     �     �     �  $   �  !        *  +   I     u  !   �  �   �     A	                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Caarmi, 2022
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Смена обоев через (мин) О приложении Все файлы Применить По центру Закрыть Папка по умолчанию Ошибка Растянуть Справка Изображения Нет обоев Настройки Случайные обои Случайные обои по таймеру Масштабировать Выбор цвета Выбор папки Выбор папки... Выбор изображения... Выбор изображения Выбор цвета фона Установить изображение Постоянный Успешно обновлено Это приложение Antix для установки обоев для предустановленных оконных менеджеров Обои antiX 