��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �           7   %     ]     m          �  
   �     �  
   �  
   �     �     �     �     �     
          ;     O     a     y     �     �     �     �     �     �  e        y                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2022-2023
Language-Team: Hebrew (Israel) (http://www.transifex.com/anticapitalista/antix-development/language/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 זמן בין מחזורי החלפת רקע (דקות) על אודות כל הקבצים החלה מירכוז סגירה תיקיה ברירת מחדל שגיאה מילוי עזרה תמונות בלי רקע אפשרויות רקע אקראי רקע אקראי מתוזמן התאמת גודל בחירת צבע בחירת תיקייה בחירת תיקייה… בחירת תמונה… בחירת תמונה בחירת צבע רקע הגדרת תמונה דומם עודכן בהצלחה זה יישום של antiX להגדרת רקע על מנהלי החלונות שהותקנו מראש תמונות רקע של antiX 