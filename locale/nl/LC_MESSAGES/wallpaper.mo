��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �           0   �     �     �  	   �     �     �     �     
                    #     ;     B  (   b     �     �     �     �     �     �     �                  i   5     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Eadwine Rose, 2014,2021
Language-Team: Dutch (http://www.transifex.com/anticapitalista/antix-development/language/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
   Tijd tussen desktopachtergrondwissel (Minuten) Over Alle Bestanden Toepassen Midden Sluiten Standaard Map Fout Vullen Hulp Beelden Geen Desktopachtergrond Opties Willekeurige Desktopachtergrond Willekeurige Getimede Desktopachtergrond Schaal Selecteer Kleur Selecteer Map Selecteer Map... Select Afbeelding... Selecteer Afbeelding Stel achtergrondkleur in Stel Afbeelding in Statisch Succesvol geüpdatet Dit is een antiX applicatie om de Desktopachtergrond op voorgeinstalleerde windows managers in te stellen antiX Desktop achtergrond 