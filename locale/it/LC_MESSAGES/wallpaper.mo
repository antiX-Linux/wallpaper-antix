��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    )   �                    !     (     /     D     K     R     X     a     r     z     �     �     �     �     �     �                 4     E     M  `   e     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2021
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Tempo tra il cambio dello sfondo (minuti) Info Tutti i file Applica Centra Chiudi Cartella predefinita Errore Riempi Aiuto Immagini Nessun wallpaper Opzioni Wallpaper casuale Wallpaper casuale a rotazione Scala Seleziona colore Seleziona cartella Seleziona cartella... Seleziona immagine... Seleziona immagine Seleziona il colore dello sfondo Imposta immagine Statico Aggiornato con successo Questa è un'applicazione antiX per impostare il wallpaper sui gestori di finestre preinstallati Sfondo antiX 