��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    '   �     �  
   �     �     �     �                         "     )     9     I  !   \     ~     �     �     �     �  
   �     �     �     �     �  i        q                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2021
Language-Team: Swedish (http://www.transifex.com/anticapitalista/antix-development/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Tid mellan wallpaper-rotation (Minuter) Om Alla Filer Använd Centrera Stäng Standard-katalog Fel Fyll Hjälp Bilder Inget Wallpaper Valmöjligheter Slumpvis Wallpaper Tidsinställda Slumpvis Wallpaper Skala Välj Färg Välj Katalog Välj Katalog... Välj Bild... Välj Bild Välj bakgrundsfärg Ställ in bild Statisk Lyckad uppdatering Detta är en antiX-applikation för att ställa in wallpaper för de förinstallerade fönster-hanterarna antiX Wallpaper 