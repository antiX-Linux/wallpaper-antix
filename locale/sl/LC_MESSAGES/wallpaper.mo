��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �       �  
             )  
   2     =     C     Q     X     g     n     t     �     �     �     �     �     �     �     �               )  	   6     @  Q   V     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2019,2021-2022
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Čas prikaza ozdaja (minute) O programu Vse datoteke Uveljavi Na sredino Zapri Privzeta mapa Napaka Zapolni zaslon Pomoč Slike Brez ozadja Opcije Naključno ozadje Naključno ozadje s časovnikom Prilagodi velikost Izbira barve Izberite mapo Izbira mape... Izbira slike... Izberite sliko Izbira barve ozadja Izbira slike Statično Uspešno posodobljeno To je antX program za spreminjanje ozadja in prednameščenih upravljalnikih oken antiX ozadje 